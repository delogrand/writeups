# Writeup Template
By Andy
___


| Problem Info | Value                           |
| ------------ | ------------------------------- |
| Problem      | problem name                    |
| CTF          | examplectf                      |
| year         | 2020                            |
| points       | 69                              |
| catagory     | misc, crypto                    |
| tags         | chinese remainder theorem, pcap |


## Summary
The given binary was decompiled in Ghidra where I found... (a few sentences outlining what how it was solved)

## Problem
Paste the problem statement here. Put the problem files in the folder where the problem is. If any files need explanation, here is also the place

## Solution
Step by step walk through of how the problem was solved. be detailed and didactic. Please include code block and pictures if necessary, any formatting is ok, but don't use the headings "#" or "##"

You want to record the sequence of thoughts that lead you to your conclusion. This can help reader see why you did what you did. You want to let the readers know how you knew to try something out or investigate something further. If you want, feel free to link to websites explaining a concept you used in solving the problem or a resource that you found that helped you solve it.

See the example.


## Conclusion
Because the function only checked X, I was able to satisfy its requirements and ... Just a quick conclusion. More relevant for lengthy problems. Totally optional for shorter problems





