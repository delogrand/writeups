# 042

## TAMUCTF 2019

**Category:** Reversing

**Points:** 500

**Description:**

> Cheers for actual assembly!

## Write-up

Looking through the [provided .s file](./reversing3.s), we can identify a block that looks like it might contain some ascii values.

```assembly
movb	$65, -16(%rbp)
movb	$53, -15(%rbp)
movb	$53, -14(%rbp)
movb	$51, -13(%rbp)
movb	$77, -12(%rbp)
movb	$98, -11(%rbp)
movb	$49, -10(%rbp)
movb	$89, -9(%rbp)
```
Sure enough, these translate to A553MblY, making our flag **gigem{A553MblY}**.

-R^2