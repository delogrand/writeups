# Snakes Over Cheese

## TAMUCTF 2019

**Category:** Reversing

**Points:** 100

**Description:**

> What kind of file is this?

## Write-up

We are given a [pyc file](./reversing2/pyc). This is Python bytecode, so we can decompile it with a website like [this one](https://python-decompiler.com/). We now have some useful python code to look at:

```Python
# Embedded file name: reversing2.py
# Compiled at: 2018-10-07 19:28:58
from datetime import datetime
Fqaa = [102, 108, 97, 103, 123, 100, 101, 99, 111, 109, 112, 105, 108, 101, 125]
XidT = [83, 117, 112, 101, 114, 83, 101, 99, 114, 101, 116, 75, 101, 121]

def main():
    print 'Clock.exe'
    input = raw_input('>: ').strip()
    kUIl = ''
    for i in XidT:
        kUIl += chr(i)

    if input == kUIl:
        alYe = ''
        for i in Fqaa:
            alYe += chr(i)

        print alYe
    else:
        print datetime.now()


if __name__ == '__main__':
    main()
```

So we don't have to mess with any translation of decimal ascii representations, we can just set the input variable, *input*, to what it seems to be looking for, *kUIl*, and run.

The program outputs **flag{decompile}**.

-R^2