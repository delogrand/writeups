# Short Circuit : Misc

#### Work by Addison Whitney

## Presentation of problem
> - Start from the monkey's paw and work your way down the high voltage line, for every wire that is branches off has an element that is either on or off. 
> - Ignore the first bit. 
> - Standard flag format.



![Original Short Circuit Schematic](./Original_Short_Circut_Schematic.jpg)



## Solution
This image is a schematic, where some LED's will not turn on based on the LED's orientation and its relationship to the high voltage line and ground. 



Below is a labeled schematic and a list of all of the LEDs and weather it was On - 1 or Off - 0.

![Labeled Short Circuit Schematic](./LED_Labled_Short_Circut_Schematic.jpg)

| LED Label | State | LED Label | State |
| --------- | ----- | --------- | ----- |
| A         | 0     | AE        | 0     |
| B         | 1     | AF        | 1     |
| C         | 1     | AG        | 1     |
| D         | 0     | AH        | 1     |
| E         | 0     | AI        | 0     |
| F         | 1     | AJ        | 1     |
| G         | 1     | AK        | 0     |
| H         | 0     | AL        | 1     |
| I         | 0     | AM        | 0     |
| J         | 1     | AN        | 1     |
| K         | 1     | AO        | 0     |
| L         | 0     | AP        | 0     |
| M         | 1     | AQ        | 0     |
| N         | 1     | AR        | 1     |
| O         | 0     | AS        | 1     |
| P         | 0     | AT        | 0     |
| Q         | 1     | AU        | 1     |
| R         | 0     | AV        | 1     |
| S         | 0     | AW        | 1     |
| T         | 1     | AX        | 1     |
| U         | 1     | AY        | 0     |
| V         | 0     | AZ        | 0     |
| W         | 1     | BA        | 1     |
| X         | 1     | BB        | 1     |
| Y         | 0     | BC        | 1     |
| Z         | 1     | BD        | 1     |
| AA        | 1     | BE        | 1     |
| AB        | 1     | BF        | 1     |
| AC        | 0     | BG        | 0     |
| AD        | 1     | BH        | 1     |



Using the table above, assign a 1 or a 0 to each branch of the high voltage line. 



![Final Short Circuit Diagram](./Final_Short_Circut_Schematic.jpg)



Record this binary, starting at the monkeys paw and working down the high voltage line.

```
00110011 00110110 00110000 10110011 10111101 10110111 10111011 10110110 10111100 10110100 00110000 10110111 00110010 00111110 1
```



Remove the 1st bit of the binary.

```
01100110 01101100 01100001 01100111 01111011 01101111 01110111 01101101 01111001 01101000 01100001 01101110 01100100 01111101
```



Convert the binary to ASCII to reveal the flag.

```
flag{owmyhand}
```

