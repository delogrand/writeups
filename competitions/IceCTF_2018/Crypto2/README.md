# Crypto 2 - Thinking outside the key!
### Work by Albert Taglieri

## Description
Estelle was messing around with her computer and she ended up outputting some garbage! Could you figure out what this means?!

Note: The flag is of the format IceCTF{<text>} where <text> is the decrypted text

## Progress
We are given a text file with the following text:

	iâ‡§fjag8â‡§qvâ‡§qy4â‡§dag8k0qâ‡§ptag86â‡§sâ‡§hecâ‡§lâ‡§c4ag8z3ssag8â‡§q7y66b
	
I noticed that the characters 'â‡§' aways occured in sequence, so I am guessing that they are some sort of 
separator or control sequence.  I'll replace them with underscores to make clearer:

	i_fjag8_qv_qy4_dag8k0q_ptag86_s_hec_l_c4ag8z3ssag8_q7y66b
	
This looks a lot cleaner and we can probably work with it.  