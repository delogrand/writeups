

def index_of(character, array):
    i = 0
    while(i < len(array)):
        if(character == array[i]):
            return i
        i += 1
    return -1

def vigenere_minus(key_array, ciphertext_string):
    lower_alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    upper_alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    ctext_array = []
    for character in ciphertext_string:
        ctext_array.append(character)

    print('ctext array is: ' + str(ctext_array))

    ptext_array = []
    i = 0
    while(i < len(ctext_array)):
        character = ctext_array[i]
        index = index_of(character, lower_alpha)
        case = 'L'
        if(index == -1):
            index = index_of(character, upper_alpha)
            case = 'U'
        if(index == -1):
            ptext_array.append(character)
        else:
            print('we are looking for index: ' + str((26 + index - key_array[i%len(key_array)])%26))
            if(case == 'L'):
                ptext_array.append(lower_alpha[(26 + index - key_array[i%len(key_array)])%26])
            else:
                ptext_array.append(upper_alpha[(26 + index - key_array[i%len(key_array)])%26])
        i += 1
    return ''.join(ptext_array)

print(vigenere_minus([0, 7, 2, 7, 1, 9, 7, 8], 'IjgJUOPLOUVAIRUSGYQUTOLTDSKRFBTWNKCFT'))
