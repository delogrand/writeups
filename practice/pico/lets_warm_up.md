# Writeup Template
By Ameen Khan
___


| Problem Info | Value         |
| ------------ | ------------- |
| Problem      | Let's Warm Up |
| CTF          | picoCTF       |
| Year         | 2019          |
| Points       | 50            |
| Category     | misc          |
| Tags         | ascii         |



## Summary

Convert given hexadecimal value to ascii character.

## Problem

>     If I told you a word started with 0x70 in hexadecimal, what would it start with in ASCII? 
>     

## Solution

Go to asciitohex.com

Enter 70 into the hexadecimal section and press convert.

## Conclusion

Used asciitohex.com to change the value 0x70 in hex to 'p' in ASCII.

picoCTF{p}



