#include <stdio.h>
#include <string.h>

int main(){
  int hex;
  int out = 0;
  char var[] = "0d25\0";
  for (int i = 0; i<strlen(var); i++){
    if(var[i] >= '0' && var[i]<='9'){
      hex = var[i] - '0';
    }
    else if (var[i] >= 'a' && var[i] <= 'f'){
      hex = var[i] - 'a' + 10;
    }
    else if (var[i] >= 'A' && var[i] <= 'F'){
      hex = var[i] - 'A' + 10;
    }
    out = 16 * out +hex;
  }
  printf("Out: %d\n", out);
  return 0;
}
