#include <stdio.h>

int main() {
	float f, c, up, lo, st;
	lo = 0;
	up = 100;
	st = 20;
	for (c = lo; c <= up; c += st){
	  f = (9.0/5.0) * c +32;
	  printf("%3.0f %6.1f\n", c, f);
	}
}
