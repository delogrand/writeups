# bandit2.3
By Wesley Hughes
___


| Problem Info | Value              |
| ------------ | ------------------ |
| Problem      | Bandit2.3          |
| CTF          | OverTheWire/Bandit |
| year         | 2020               |
| points       | 0                  |
| category     | practice           |
| tags         | cat, space         |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored in a file called **spaces in this filename** located in the home directory

## Solution
I remembered that to access or do something with a file that has spaces in it, you can simply enclose it in quotes as follows: cat "spaces in this filename" which gave me the password - UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK


## Conclusion
Short problem





