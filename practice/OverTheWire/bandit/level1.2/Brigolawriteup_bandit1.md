Bandit0 Writeup
By Massimo Brigola
___


| Problem Info | Value                           |
| ------------ | ------------------------------- |
| Problem      | Bandit1              	         |
| CTF          | 		                          |
| year         | 2020                            |
| points       |                                  |
| category     | training	                     |
| tags         | path, cat		                |


## Summary
I solved this level by researching how to concatenate a file named '-'.

## Problem
The password for the next level is stored in a file called **-** located in the home directory

## Solution
After some research, I found that one way to concatenate a file named '-' is to just put the file path in front of it. In this case, I just used the command 'cat ./-' to find the password.

## Conclusion
I think this qualifies as a short problem, so no conclusion!




