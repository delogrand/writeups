# Bandit Level 11
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 11 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | tr                          |


## Summary
I had to learn how to use the command 'tr' to translate the data.txt file by 13 characters for both lowercase and uppercase letters. I also had to use piping to be able to do this.

## Problem
The password for the next level is stored in the file **data.txt**, where all lowercase (a-z) and uppercase (A-Z) letters have been rotated by 13 positions

## Solution
First I had to login to level 11 by typing the command below and then typing in the password:

ssh bandit11@bandit.labs.overthewire.org -p 2220

Since this level just involves a Caesar Cypher, I assumed that there would be a relatively simple command to be able to do this. I then just went through the manual page of the recommended commands to find a command that seemed like it would help. It seemed that the 'tr' command would be my best bet. After some Googling around, I figured out the correct way to use the command so I had to input:

cat data.txt | tr '[a-z]' '[n-za-m]' | tr '[A-Z]' '[N-ZA-M]'

This command printed out the password.


## Conclusion
This was helpful for learning how to use shift letters in a file and gave more practice with piping.





