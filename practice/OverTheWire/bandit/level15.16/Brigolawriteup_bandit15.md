# Bandit Level 15
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 15 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | openssl, s_client           |


## Summary
This problem just involved creating a SSL connection to another server then typing in the password.

## Problem
The password for the next level can be retrieved by submitting the password of the current level to **port 30001 on localhost** using SSL encryption.

## Solution
First I had to login to level 15 by typing the command below and then typing in the password:

ssh bandit15@bandit.labs.overthewire.org -p 2220

The only command needed to solve this problem is 'openssl s_client -connect localhost:30001' and then you will be connect to the local machine on port 30001. After that, you just have to paste in the password to login to level 15 and it will output the password for the next level.


## Conclusion
This problem was pretty easy for me because I stumbled across this method of connection on the last level before I realized that I was going down the wrong path. I already knew which command I needed and how to use it.



