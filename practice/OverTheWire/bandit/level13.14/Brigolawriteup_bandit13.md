# Bandit Level 13
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 13 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | ssh                         |


## Summary
This problem was pretty deceiving for a while. I thought it would be a much more complicated solution than it actually was. It turns out that the only thing needed for this problem was using  the 'ssh' command with an argument. I had to do some outside research and read the ssh manual page several times over.

## Problem
The password for the next level is stored in **/etc/bandit_pass/bandit14 and can only be read by user bandit14**. For this level, you don’t get the next password, but you get a private SSH key that can be used to log into the next level. **Note:** **localhost** is a hostname that refers to the machine you are working on

## Solution
First I had to login to level 13 by typing the command below and then typing in the password:

ssh bandit13@bandit.labs.overthewire.org -p 2220

The only command needed to solve this problem is 'ssh bandit14@localhost -i sshkey.private' and then you will be logged into bandit14. 


## Conclusion
This was a good lesson for using private keys as substitutes for passwords. I definitely over thought this problem and started to delve into the 'openssl' command before realizing that I was probably heading down the wrong track. It was also a good introduction to how local machines work.





