# bandit13.14
By Wesley Hughes
___


| Problem Info | Value                                    |
| ------------ | ---------------------------------------- |
| Problem      | Bandit13.14                              |
| CTF          | OverTheWire/Bandit                       |
| year         | 2020                                     |
| points       | 0                                        |
| category     | practice                                 |
| tags         | ssh, telnet, nc, openssl, s_client, nmap |


## Summary
SSH'd directly into bandit14 from bandit13 user.

## Problem
The password for the next level is stored in **/etc/bandit_pass/bandit14 and can only be read by user bandit14**. For this level, you don’t get the next password, but you get a private SSH key that can be used to log into the next level. **Note:** **localhost** is a hostname that refers to the machine you are working on

## Solution

I've done set-up with sshkeys before so I know all I had to do was ssh bandit14@localhost -i sshkey.private to login to the next level.


## Conclusion
Short problem
