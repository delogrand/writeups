# bandit5.6
By Wesley Hughes
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | Bandit5.6                   |
| CTF          | OverTheWire/Bandit          |
| year         | 2020                        |
| points       | 0                           |
| category     | practice                    |
| tags         | ls, cd, cat, file, du, find |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored in a file somewhere under the **inhere** directory and has all of the following properties:

- human-readable
- 1033 bytes in size
- not executable

## Solution
There were lots of human-readable and non-executable files in the entire directory. To easily find the file, I instead used the command find -type f -size 1033c to find the one file that was exactly 1033 bytes. I then opened that file to find the following password: DXjZPULLxYr17uwoI01bNLQbtFemEgo7


## Conclusion
Short problem





