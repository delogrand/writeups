# bandit8.9
By Wesley Hughes
___


| Problem Info | Value                                                        |
| ------------ | ------------------------------------------------------------ |
| Problem      | Bandit8.9                                                    |
| CTF          | OverTheWire/Bandit                                           |
| year         | 2020                                                         |
| points       | 0                                                            |
| category     | practice                                                     |
| tags         | grep, sort, uniq, strings, base64, tr, tar, gzip, bzip2, xxd |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored in the file **data.txt** and is the only line of text that occurs only once

## Solution

I passed the command sort data.txt into uniq -u by doing sort data.txt | uniq -u to get the one unique line which is the following password:

UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR


## Conclusion
Short problem





