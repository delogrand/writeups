# Writeup Template
By Andy
___


| Problem Info | Value  |
| ------------ | ------ |
| Problem      | Bandit |
| CTF          | Bandit |
| year         | 2024   |
| points       |        |
| catagory     |        |
| tags         | {Boi}  |


## Summary
Bandit level 0 problem

## Problem
The goal of this level is for you to log into the game using SSH. The host to which you need to connect is **bandit.labs.overthewire.org**, on port 2220. The username is **bandit0** and the password is **bandit0**. Once logged in, go to the [Level 1](https://overthewire.org/wargames/bandit/bandit1.html) page to find out how to beat Level 1.

## Solution
enter the domain by the ssh command and port and the given password


## Conclusion
I just put in what was given, accidentally put brackets in name





