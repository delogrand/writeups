Bandit0 Writeup
By Massimo Brigola
___


| Problem Info | Value                           |
| ------------ | ------------------------------- |
| Problem      | Bandit0              	         |
| CTF          | 		                         |
| year         | 2020                            |
| points       | 0                               |
| category     | training	                     |
| tags         | cat, ssh		     |


## Summary
I solved this level by using the 'ssh' command in Windows PowerShell to connect to the first bandit.overthewire.org host.

## Problem
The goal of this level is for you to log into the game using SSH. The host to which you need to connect is bandit.labs.overthewire.org, on port 2220. The username is bandit0 and the password is bandit0. Once logged in, go to the Level 1 page to find out how to beat Level 1.

## Solution
The first thing I had to do was learn how to connect to a Secure Shell, so I clicked on the wikiHow link on the Bandit Level 0 page. After reading this article, I found that I just needed to use the command "ssh bandit0@bandit.labs.overthewire.org -p 2220" then type the password which was bandit0. I also learned that I cannot be on eduwireless while trying to connect.

## Conclusion
I think this qualifies as a short problem, so no conclusion!




