# bandit0
By Wesley Hughes
___


| Problem Info | Value              |
| ------------ | ------------------ |
| Problem      | Bandit0            |
| CTF          | OverTheWire/Bandit |
| year         | 2020               |
| points       | 0                  |
| category     | practice           |
| tags         | ssh                |


## Summary
I SSH into the game using the given username and password along with the port number.

## Problem
The goal of this level is for you to log into the game using SSH

## Solution
I reviewed how to use the ssh command (ssh user@ip -p <port>). 

I was given the username/password (bandit0/bandit0) with ip bandit.labs.overthewire.org and port 2220.

I used PowerShell but then also used PuTTy just to give myself a refresher on using both methods.


## Conclusion
Short problem





