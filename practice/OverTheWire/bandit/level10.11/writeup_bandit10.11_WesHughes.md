# bandit10.11
By Wesley Hughes
___


| Problem Info | Value                                                        |
| ------------ | ------------------------------------------------------------ |
| Problem      | Bandit10.11                                                  |
| CTF          | OverTheWire/Bandit                                           |
| year         | 2020                                                         |
| points       | 0                                                            |
| category     | practice                                                     |
| tags         | grep, sort, uniq, strings, base64, tr, tar, gzip, bzip2, xxd |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored in the file **data.txt**, which contains base64 encoded data

## Solution

I did man base64 to learn how the command works. Then I did base64 -d data.txt to get the password: IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR


## Conclusion
Short problem
