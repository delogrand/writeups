# Bandit Level 22
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 22 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | cron, crontab               |

## Summary

To solve this problem, I had to analyze a script written by someone else to see how it works. Then, I had to modify it to fit my needs.

## Problem
A program is running automatically at regular intervals from **cron**, the time-based job scheduler. Look in **/etc/cron.d/** for the configuration and see what command is being executed.

**NOTE:** Looking at shell scripts written by other people is a very useful skill. The script for this level is intentionally made easy to read. If you are having problems understanding what it does, try executing it to see the debug information it prints.

## Solution
After logging into the level, I did 'ls' to see what files were available to me. It showed nothing so I just did 'cd /etc/cron.d/' to view the configuration of jobs. I saw a bandit23 file so I did 'cat /usr/bin/cronjob_bandit23.sh' and it showed the script for the job. I realized that the script would only target the user that I was currently logged in as, so I had to copy and paste the code from the script into my terminal and change the user to bandit23. Executing this command then gave me the directory that the password for the next password was sent to. I then did 'cat /tmp/8ca319486bfbbc3663ea0fbe81326349' to print out the next password.


## Conclusion
This level was not too hard because the code script was very easy to read. The only complication was realizing that I could just copy and paste it into my command line.