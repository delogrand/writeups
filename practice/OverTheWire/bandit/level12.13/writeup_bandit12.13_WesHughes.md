# bandit12.13
By Wesley Hughes
___


| Problem Info | Value                                          |
| ------------ | ---------------------------------------------- |
| Problem      | Bandit12.13                                    |
| CTF          | OverTheWire/Bandit                             |
| year         | 2020                                           |
| points       | 0                                              |
| category     | practice                                       |
| tags         | tr, tar, gzip, bzip2, xxd, mkdir, cp, mv, file |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored in the file **data.txt**, which is a hexdump of a file that has been repeatedly compressed. For this level it may be useful to create a directory under /tmp in which you can work using mkdir. For example: mkdir /tmp/myname123. Then copy the datafile using cp, and rename it using mv (read the manpages!)

## Solution

I had to reverse the hexdump and then constantly rename, unzip, rename, unarchive, rename, unzip, etc until I got a file that had ASCII text again. This resulted in the password: 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL




## Conclusion
Short problem
