# Bandit Level 4
By Massimo Brigola
___


| Problem Info | Value                      |
| ------------ | -------------------------- |
| Problem      | OverTheWire Bandit Level 4 |
| CTF          | Bandit                     |
| year         | 2020                       |
| points       | 0                          |
| category     | training, bandit           |
| tags         | file                       |


## Summary
The main thing I had to research to solve this level was how to use the file command to show the files types of all of the files in a directory. I found that it was as simple as using an asterisk to designate listing all files in a directory.

## Problem
The password for the next level is stored in the only human-readable file in the **inhere** directory. Tip: if your terminal is messed up, try the “reset” command.

## Solution
First I had to login to level 4 by typing the command below and then typing in the password:

ssh bandit4@bandit.labs.overthewire.org -p 2220

I then used the command 'ls' to list the directories and find the directory name to switch into. After I used the command 'cd inhere' to get into the desired directory, I then read up on the manual page for the 'file' command and did some outside research. I found that it was as easy to just do 'file ./*' to list all of the files and their file types in a directory. The only file that was of type ASCII text was '-file07' so I just did 'cat ./-file07' to print out the password.


## Conclusion
This was helpful for learning about the file command, even though it was a quick solution.





