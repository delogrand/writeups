# bandit0.1
By Wesley Hughes
___


| Problem Info | Value              |
| ------------ | ------------------ |
| Problem      | Bandit1            |
| CTF          | OverTheWire/Bandit |
| year         | 2020               |
| points       | 0                  |
| category     | practice           |
| tags         | ssh, ls, cat       |


## Summary
Read the file to get the password then ssh'd into the next level.

## Problem
The password for the next level is stored in a file called **readme** located in the home directory. Use this password to log into bandit1 using SSH.

## Solution
I used ls to see if the file was in the directory I was in after ssh-ing in for bandit0. Once I saw readme I used cat to read the file to obtain the password, "boJ9jbbUNNfktd78OOpsqOltutMc3MY1", then logged into the next level with ssh bandit1@.... and I input the new password.


## Conclusion
Short problem





