# Bandit Level 0
By BKM3 (Brian Martin III)
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | Level 0                     |
| CTF          |                             |
| Year         | 2020                        |
| Points       | 0                           |
| Category     |                             |
| Tags         |                             |

## Summary

Complete level 0.

## Problem

We have to log into the game from powershell.

## Solution

Open powershell. Use the command "ssh -p 2220 bandit0@bandit.labs.overthewire.org"
Enter the password, then boom you're in.

#### Exploring

#### Reversing

#### Exploiting

#### Ghidra
 
## Conclusion

SSH into the game, then you're into the game ;).





