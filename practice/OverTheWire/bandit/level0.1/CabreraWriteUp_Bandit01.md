# Bandit Level 0
By Nicholas Cabrera
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | Level 0                     |
| CTF          |                             |
| Year         | 2020                        |
| Points       | 0                           |
| Category     | Training                    |
| Tags         | ssh, cat                    |

## Summary

The task is to complete Level 0.

## Problem

Using Windows Powershell, log into bandit level 0.

## Solution

Open "Windows Powershell"
Change colors to the cool colors (very important) 
Enter the command "ssh -p 2220 bandit0@bandit.labs.overthewire.org"
Copy & Paste the password in, be careful though it is invisible!
I'm in.

#### Exploring

#### Reversing

#### Exploiting

#### Ghidra
 
## Conclusion

Using the ssh command on powershell, log into level 0.




