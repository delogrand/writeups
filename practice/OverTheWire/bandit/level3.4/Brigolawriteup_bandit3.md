# Bandit Level 3
By Massimo Brigola
___


| Problem Info | Value                      |
| ------------ | -------------------------- |
| Problem      | OverTheWire Bandit Level 3 |
| CTF          | Bandit                     |
| year         | 2020                       |
| points       | 0                          |
| category     | training, bandit           |
| tags         | cat, ls, cd                |


## Summary
The main thing I had to research to solve this level was how to show hidden filenames so that I could use the filename in a 'cat' command.

## Problem
The password for the next level is stored in a hidden file in the **inhere** directory.

## Solution
First I had to login to level 3 by typing the command below and then typing in the password:

ssh bandit3@bandit.labs.overthewire.org -p 2220

I then used the command 'ls' to list the directories and find the directory name to switch into. After I used the command 'cd inhere' to get into the desired directory, I then tried 'ls' to see if anything popped up (and nothing did). I decided to use the command 'man ls' to see if there was a way to reveal hidden files. Luckily, there was an argument that I could use to reveal hidden files so I just used the command 'ls -a' to show all files. I then just did 'cat .hidden' to show the password/


## Conclusion
This was a helpful problem to learn more about manual pages and using arguments with commands.





