# Bandit Level 14
By Massimo Brigola
___


| Problem Info | Value                       |
| ------------ | --------------------------- |
| Problem      | OverTheWire Bandit Level 14 |
| CTF          | Bandit                      |
| year         | 2020                        |
| points       | 0                           |
| category     | training, bandit            |
| tags         | telnet                      |


## Summary
This problem (like the last one) was a lot more simple than I thought it would be. It took me a while to see that host name and port was an argument for the telnet command. The telnet command was the only thing needed to complete the level

## Problem
The password for the next level can be retrieved by submitting the password of the current level to **port 30000 on localhost**.

## Solution
First I had to login to level 14 by typing the command below and then typing in the password:

ssh bandit14@bandit.labs.overthewire.org -p 2220

The only command needed to solve this problem is 'telnet localhost 30000' and then you will connect to the local machine on port 30000. After that, you just have to paste in the password to login to level 14 and it outputted the password for the next level.


## Conclusion
The layout of the manual pages can sometimes be a bit confusing because it took me a while to see that host and port were an argument to be used with telnet, I was only looking in the options section.



