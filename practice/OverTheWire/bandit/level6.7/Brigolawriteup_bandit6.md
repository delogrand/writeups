# Bandit Level 6
By Massimo Brigola
___


| Problem Info | Value                      |
| ------------ | -------------------------- |
| Problem      | OverTheWire Bandit Level 6 |
| CTF          | Bandit                     |
| year         | 2020                       |
| points       | 0                          |
| category     | training, bandit           |
| tags         | find, cd                   |


## Summary
For the most part, this level just involved using the find command and some arguments to search for a file that had certain properties. The other thing I had to learn was how to change directories up a level so that I could search the entire server.

## Problem
The password for the next level is stored **somewhere on the server** and has all of the following properties:

- owned by user bandit7
- owned by group bandit6
- 33 bytes in size

## Solution
First I had to login to level 6 by typing the command below and then typing in the password:

ssh bandit6@bandit.labs.overthewire.org -p 2220

Since I knew that the file was going to be 'somewhere on the server', I could pretty much assume that it was not going to be in this directory so I learned to use 'cd ..' to go up a directory. I then did this command again to get to the main directory. From there, I just had to do 'find -size 33c -user bandit7 -group bandit6' to filter down my search. Sadly, I got a long list of files as an output. Luckily though, I noticed that only one of those files did not have a 'permission denied' tag next to it, so I figured this was the file I needed. I just did a 'cat' command into that file to get the password.


## Conclusion
This was helpful for learning how to navigate through a server and across directories.





