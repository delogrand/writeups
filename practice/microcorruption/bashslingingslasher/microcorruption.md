# microcorruption

**by firef0x**

## Assumptions

* you don't really need to know much assembly (but this is a good way to learn for the bold and determined)
* that you know some basic rev/pwn techniques
* you do not need to know much about gdb but it is very helpful

## Tutorial

The tutorial is pretty helpful at showing you the interface and basic commands. It also provides an introduction to their gdb-like debugger. It also exposes you to the basics of debugging. It walks you through step by step, showing you the logic of hacking the first problem. The password is just "password"

## New Orleans

The `create_password` function write the password to memory starting at 0x2400. The check password function compares your password to the password in memory step by step, then returns 0x1 if correct. the password at 0x2400 is `uQ5ZNW0`. 

## Sydney

The intro claims to have removed the password from memory. The important function again is the check password function. First, I'll put in some random password like "abcd" and step through there to see how the check password function works.

Our password starts at 0x439c. The first step compares 0x4258 with the first two bytes of our password in the line `cmp	#0x4258, 0x0(r15)`. the next line jumps to a section of password that puts 0 on the return register and returns. we don't want that.

```assembly
clr	r14
mov	r14, r15
ret
```

Looking more at the function, it makes this comparison several time with new hex bytes. If you pass all the comparisons, then the code jumps to move `r14` (containing 0x1) to `r15` and returns, signifying a correct password.

The hex characters are `425869413e5c3667` in ascii that is `BXiA>\6g`. Now the logical step is to put that in as the password, but it won't work.

That's because computers don't just store information normally in memory. There is this thing called endian-ness. This system is little endian. Read about endian-ness [here](https://chortle.ccsu.edu/AssemblyTutorial/Chapter-15/ass15_3.html). This means that the password is actually `XBAi\>g6` because the set of 4 bytes are backward when they are loaded.

Turns out the password wasn't in memory. It was in the instructions.

## Reykjavik

This level boasts "military grade encryption," we'll see about that.

Reading the main reveals two function calls. One is called `enc`, so it probably encrypts. I'll start by debugging it.

First, it builds a little list in memory with this code

```assembly
clr	r13
mov.b	r13, 0x247c(r13)	; jne jumps back to this line
inc	r13
cmp	#0x100, r13
jne	#0x4490 <enc+0xa>
mov	#0x247c, r12
```

Set a breakpoint after and use c to continue to that break point. So starting at 0x247c, these lines build a table from 0x00 to 0xff. 

The next interesting bit of assembly is this:

```asm
mov.b	@r12, r8			; jump brings you here
mov.b	r8, r10
add	r10, r13
mov	r11, r10
and	#0xf, r10
mov.b	0x4472(r10), r10	; gets next character of "ThisIsSecureRight?" starting at 0x4472 to register 10
sxt	r10
add	r10, r13
and	#0xff, r13				; chops it down to two bytes
mov	r13, r10
add	#0x247c, r10			; uses the number r13 and the address of the list to create a new address
mov.b	@r10, r9			; grabs that addresss from the above line and shoves it in r9
mov.b	r8, 0x0(r10)
mov.b	r9, 0x0(r12)
inc	r11
inc	r12
cmp	#0x100, r11
jne	#0x44a4 <enc+0x1e>
```

It looks like this code is writing over the list created with new values. 0x00, 0x01, 0x02 become 0x54 0xbd, 0x28. Maybe this list will be the look up table for the encryption?

```assembly
inc	r12
and	#0xff, r12
mov	r12, r10
add	#0x247c, r10
mov.b	@r10, r8
add.b	r8, r11
mov.b	r11, r11
mov	r11, r13
add	#0x247c, r13
mov.b	@r13, r9
mov.b	r8, 0x0(r13)
mov.b	r9, 0x0(r10)
add.b	@r13, r9
mov.b	r9, r13
xor.b	0x247c(r13), 0x0(r15)
inc	r15
add	#-0x1, r14
tst	r14
jnz	#0x44dc <enc+0x56>
```

Looks like another loop that writes to memory again. Use a breakpoint to get past it.

The next function called 0x2400. This is unusual as this is part of the memory that is not in the instruction section. The current instruction box will be very helpful here.

```assembly
[forgot to record]
push r4
mov sp, r4
add #0x4, r4
add #0xffe0, sp
mov #0x4520, r11
jmp $+0x10
mov.b @r11, r15
tst.b r15
jnz $-0x12
inc r11
sxt r15
push r15
push #0x0
call #0x2464		; calls another function from mem
```

Let's get the other function using the current instruction box.

```assembly
mov 0x2(sp), r14
push sr
mov r14, r15
swpb r15
mov r15, sr
bis #0x8000, sr
call #0x10
```

Hopefully this ends soon

### call 0x10

```assembly
rrc pc	; returns pc to 0x2478
...
ret
add #0x4, sp
mov.b @r11, r15
tst.b r15		;0x68 at first execution
jnz $-0x12
inc r11
... continues to loop to print "what's the password?"
```

### after putting in password

The stack pointer is then moved to the location in memory of the first byte of the text you put in the password box. You can find the password in memory as you can put in a unique string. the sp box shows that the stack points to the first box.

```assembly
cmp #0x8aa1, -0x24(r4)
jnz $+0xc
add #0x20, sp
```

The first time, the jump failed and the program terminated. It compares the first two bytes of your password with 0x8aa1. Putting that in as the your password (a18a hex encoded) proceeds. 

```assembly
cmp #0x8aa1, -0x24(r4)
jnz $+0xc
push #0x7f
call #0x2464
```

### function 0x2464

```assembly
mov 0x2(sp), r14	; 7f to r14
push sr
mov r14, r15
swpb r15			; so 007f becomes 7f00
mov r15, sr
bis #0x8000, sr		; sr goes from 7f00 to ff00\
call #0x10
```

Wow. the door unlocked. so much for military grade encryption. I wasted a ton of time when the code was just in one instruction. sigh. 

### Lesson Learned

Go through the whole program before trying to analyze it to get a big picture idea.

password is 0xa18a

## Hanoi

Looks like there's something new

> ```
> LockIT Pro Hardware  Security Module 1 stores  the login password, ensuring users  can not access  the password through  other means. The LockIT Pro  can send the LockIT Pro HSM-1  a password, and the HSM will  return if the password  is correct by setting  a flag in memory.
> ```

Going through the code shows that the password is indeed checked by an external function triggered by an interrupt.

```assembly
4454 <test_password_valid>
...
push	r14
push	r15
push	#0x7d
call	#0x457a <INT>		; checks the password
...
```

And here is the call to this test password function from the login function as well as the code that determine whether to unlock the door or not.

```assembly
4520 <login>
...
call	#0x4454 <test_password_valid>
tst	r15
jz	$+0x8
mov.b	#0xea, &0x2410
mov	#0x44d3 "Testing if password is valid.", r15
call	#0x45de <puts>
cmp.b	#0xf6, &0x2410								; if what's at 0x2410 is 0xf6, then
jne	#0x4570 <login+0x50>							; unlock door, if not, fail
mov	#0x44f1 "Access granted.", r15
call	#0x45de <puts>
call	#0x4448 <unlock_door>
...
```

If you look for the password you used in memory, you can find that it starts at 0x2400. Funny enough, the byte that the function checks to see if the password is valid is located at 0x2410, just past our password. what happens if send a password longer than 16 *like it told us not to do*. Turns out, you can write to this byte. This is called a **buffer overflow**, and we will be using it to unlock the door. Since we need this byte to by 0xf6, all we have to do is write 16 bytes of whatever and then write 0xf6. Remember to hex encode the input.

`000102030405060708090a0b0c0d0e0ff6` as hex is the password.



## Cusco

This problem is much like the last, where the lock sends the password to an external module that tells you if the password is correct or not. This one claims to have security improvements over the last version.

> This is Software Revision 02. We have improved the security of the lock by  removing a conditional  flag that could  accidentally get set by passwords that were too long.

notes, flag for the unlock is set in r15 on biggest functions exit. it takes this from mem that is before the imput in the stack so you can not run it over. you do get control of the instruction pointer with long passwords. look to see where the function pointer is and include a screenshot for this lesson.